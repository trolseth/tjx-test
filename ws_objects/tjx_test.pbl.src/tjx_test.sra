﻿$PBExportHeader$tjx_test.sra
$PBExportComments$Generated Application Object
forward
global type tjx_test from application
end type
global transaction sqlca
global dynamicdescriptionarea sqlda
global dynamicstagingarea sqlsa
global error error
global message message
end forward

global type tjx_test from application
string appname = "tjx_test"
end type
global tjx_test tjx_test

on tjx_test.create
appname="tjx_test"
message=create message
sqlca=create transaction
sqlda=create dynamicdescriptionarea
sqlsa=create dynamicstagingarea
error=create error
end on

on tjx_test.destroy
destroy(sqlca)
destroy(sqlda)
destroy(sqlsa)
destroy(error)
destroy(message)
end on

event open;open(w_test)
end event

